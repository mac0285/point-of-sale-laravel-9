<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddLogPenjualanTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('Penjualan', function (Blueprint $table) {
            //
            $table->integer('flag')->unsigned()->nullable()->after('id_member');
            $table->integer('active')->unsigned()->nullable()->after('flag');
            $table->integer('status')->unsigned()->nullable()->after('active');
            $table->integer('pembeli')->unsigned()->nullable()->after('status');
            $table->integer('img')->unsigned()->nullable()->after('status');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('Penjualan', function (Blueprint $table) {
            //
        });
    }
}
